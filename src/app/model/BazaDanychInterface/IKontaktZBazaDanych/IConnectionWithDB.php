<?php

interface IConnectionWithDB {
    public function GetDBObject(string $query);
    public function PutDBObject(string $query);
    public function ConnectToDB();
}
