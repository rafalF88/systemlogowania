<?php

require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

$request = Request::createFromGlobals();

//$controller = isset($_GET['controller']) ? $_GET['controller'] : 'home';
$controller = $request->get('controller','home');
$dir = __DIR__.'/../src/app/controllers/';


//echo $dir;

if(file_exists($dir.$controller.'.php'))
{
    ob_start();
    require $dir.$controller.'.php';
}else{
    
    header('HTTP/1.1 404 Not Found');
    echo '404';
}